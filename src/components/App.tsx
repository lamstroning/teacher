import React from "react";
import '../style/index.scss'
import {Route, Routes} from "react-router-dom";
import Page from "./Page";
import {AppRouter} from "./AppRouter";

export default function App() {
   return (
       <>
           <AppRouter />
       </>
   )
}
