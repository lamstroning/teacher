import {BottomNavigation, BottomNavigationAction} from "@mui/material";
import Restore from "@material-ui/icons/Restore";
import React from "react";
import {Link, Outlet} from "react-router-dom";
import {Group, Person, CalendarToday, Book} from "@material-ui/icons";

export default function Page() {
        return <div className='page'>
            <div className='page__body'>
                <Outlet />
            </div>
            <div className='page__bottom'>
                <BottomNavigation>
                    <Link to='/'>
                        <BottomNavigationAction
                            label='Recents'
                            value='Recents'
                            icon={<Restore/>}
                        />
                    </Link>
                    <Link to='/dashboard'>
                        <BottomNavigationAction
                            label='Group'
                            value='Group'
                            icon={<Group/>}
                        />
                    </Link>
                    <Link to='/profile'>
                        <BottomNavigationAction
                            label='Profile'
                            value='Profile'
                            icon={<Person />}
                        />
                    </Link>
                    <Link to='/calendar'>
                        <BottomNavigationAction
                            label='Calendar'
                            value='Calendar'
                            icon={<CalendarToday />}
                        />
                    </Link>
                    <Link to='/book'>
                        <BottomNavigationAction
                            label='Book'
                            value='Book'
                            icon={<Book />}
                        />
                    </Link>
                </BottomNavigation>
            </div>
        </div>
    }
