import React from "react";
import {
    Route,
    Routes
} from "react-router-dom";
import Page from "./Page";
export function AppRouter() {
    return(
        <Routes>
            <Route path="/" element={<Page />} >
                <Route index element={<div>homepage</div>} />
                <Route path="dashboard" element={<div>dashboard</div>} />
                <Route path="profile" element={<div>profile</div>} />
                <Route path="calendar" element={<div>calendar</div>} />
                <Route path="book" element={<div>book</div>} />
            </Route>
        </Routes>
    )
}

