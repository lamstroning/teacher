import ReactDOM from 'react-dom'
import {BrowserRouter, RouterProvider} from 'react-router-dom';
import React from 'react'
import App from "./components/App";

ReactDOM.render(
    <React.StrictMode>
        {/*<RouterProvider router={router} />*/}
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </React.StrictMode>,
    document.getElementById('root')
)
